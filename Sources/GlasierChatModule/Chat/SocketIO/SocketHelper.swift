//
//  SocketHelper.swift
//  Socket_demo
//
//  Created by Krishna Soni on 06/12/19.
//  Copyright © 2019 Krishna Soni. All rights reserved.
//

import UIKit
import Foundation
//import SocketIO

//http://192.168.0.50:3001 Local
//http://15.207.130.51:5000/
//192.168.0.22:5000
//socket server live URL: http://15.207.130.51:5000
//var kHost = "http://192.168.0.33:7000/"
var kHost = "http://192.168.0.33:7000/"

let kRoomCreate = "joinRoom"
let kSendMessage = "sendMessage"
let kRecieveMessage = "receiveMessage"
let kGetAllMessages = "receiveAllMessageList"
let kDeclineCall = "DeclineCall"
let kRoomUsersList = "roomUsers"

final class SocketHelper: NSObject {
    
    static let shared = SocketHelper()
    
    private var manager: SocketManager?
    private var socket: SocketIOClient?
    
    public var TYPE_MESSAGE = "1";
    public var TYPE_IMAGE = "2";
    public var TYPE_VIDEO = "3";
    public var TYPE_PDF = "4";
    public var TYPE_CALL = "5";
    
    
    override init() {
        super.init()
        configureSocketClient()
    }
    
    private func configureSocketClient() {
        
        guard let url = URL(string: kHost) else {
            return
        }
        
        manager = SocketManager(socketURL: url, config: [.log(true), .compress])
        
        socket?.on(clientEvent: .connect) {data, ack in
            print("socket connected")
        }
        
        guard let manager = manager else {
            return
        }
        
        socket = manager.socket(forNamespace: "/**********")
    }
    
    func establishConnection() {
        
        guard let socket = manager?.defaultSocket else {
            return
        }
        
        socket.connect()
        
    }
    
    func closeConnection() {
        
        guard let socket = manager?.defaultSocket else {
            return
        }
        
        socket.disconnect()
    }
    
    func stopListeningEvent(event: String) {
        guard let socket = manager?.defaultSocket else {
            return
        }
        
        socket.off(event)
    }
    
    func joinChatRoom(roomId: String, userId: String, userType: String, completion: () -> Void) {
        
        guard let socket = manager?.defaultSocket else {
            return
        }
        
        socket.emit(kRoomCreate, roomId, userId, userType) {
            print("emit succefully.....")
            
        }
        
        completion()
    }
    
    func getMessage(completion: @escaping (_ messageInfo: Message?) -> Void) {
        
        guard let socket = manager?.defaultSocket else {
            return
        }
        
        socket.on(kRecieveMessage) { (dataArray, socketAck) -> Void in
            
            guard let objDict = dataArray[0] as? NSDictionary, let arrMsg = objDict["messageList"] as? NSArray else{
                return
            }
            
            completion(Message(dictionary: arrMsg[0] as! [String: Any]))
        }
    }
    
    func getMessageHistory(completion: @escaping (_ messageInfo: [Message]) -> Void) {
        
        guard let socket = manager?.defaultSocket else {
            return
        }
        
        socket.on(kGetAllMessages) { (dataArray, socketAck) -> Void in
            
            print("Vipul Parmar",dataArray)
            
            guard let objDict = dataArray[0] as? NSDictionary, let arrMsg = objDict["messageList"] as? NSArray else{
                return
            }
            
            var arrMessage = [Message]()
            
            for obj in arrMsg {
                
                guard let newObj = obj as? [String:Any] else { return }
                
                arrMessage.append(Message(dictionary: newObj))
            }
            
            completion(arrMessage)
            
        }
    }
    
    func sendMessage(roomId: String, userId: String, message: String, messageType: String, channelName: String, token: String) {
        
        guard let socket = manager?.defaultSocket else {
            return
        }
        
        //        socket.emit(kSendMessage, roomId, userId, message, messageType, token, channelName) {
        //            print("emit succefully.....")
        //
        //        }
        
        socket.emit(kSendMessage, message, messageType) {
            print("emit succefully.....")
            
        }
        
    }
    
    func getUserList(completion: @escaping (_ userInfo: [Model_ChatUserList]?) -> Void) {
        
        guard let socket = manager?.defaultSocket else {
            return
        }
        
        socket.on(kRoomUsersList) { dataArray, socketAck -> Void in
            
            print("dataArray = ", dataArray)
            print("socketAck = ", socketAck)
            
            guard let objDict = dataArray[0] as? NSDictionary, let arrMsg = objDict["users_list"] as? NSArray else {
                return
            }
            
            var arrMessage = [Model_ChatUserList]()
            
            for obj in arrMsg {
                
                guard let newObj = obj as? [String:Any] else { return }
                
                arrMessage.append(Model_ChatUserList(dictionary: newObj))
            }
            
            completion(arrMessage)
        }
    }
    
    func declineCall(roomId: String, userId: String, isSendNotification: Bool) {
        
        guard let socket = manager?.defaultSocket else {
            return
        }
        
        socket.emit(kDeclineCall, roomId, userId, isSendNotification) {
            print("get succefully.....")
        }
        
    }
    
    func getdeclineCallStatus(completion: @escaping (_ strUserId: String) -> Void) {
        
        guard let socket = manager?.defaultSocket else {
            return
        }
        
        socket.on(kDeclineCall) { (userId, socketAck) -> Void in
            print("For Call",userId)
            completion("")
        }
    }
    
    
    //    func getTypingStatus(completion: @escaping (_ userList: [User]?) -> Void) {
    //
    //        guard let socker = manager?.defaultSocket else { return }
    //
    //        socker.on("kUserTyping") { dataArray, socketAck in
    //            print(dataArray)
    //        }
    //    }
}



//    func leaveChatRoom(nickname: String, completion: () -> Void) {
//
//        guard let socket = manager?.defaultSocket else{
//            return
//        }
//
//        socket.emit(kExitUser, nickname)
//        completion()
//    }

//    func participantList(completion: @escaping (_ userList: [User]?) -> Void) {
//
//        guard let socket = manager?.defaultSocket else {
//            return
//        }
//
//        socket.on(kUserList) { [weak self] (result, ack) -> Void in
//
//            guard result.count > 0,
//                  let _ = self,
//                  let user = result.first as? [[String: Any]],
//                  let data = UIApplication.jsonData(from: user) else {
//                return
//            }
//
//            do {
//                let userModel = try JSONDecoder().decode([User].self, from: data)
//                completion(userModel)
//
//            } catch let error {
//                print("Something happen wrong here...\(error)")
//                completion(nil)
//            }
//        }
//
//    }
