//
//  Message.swift
//  socket_demo
//
//  Created by Krishna Soni on 01/01/20.
//  Copyright © 2020 Krishna Soni. All rights reserved.
//

import Foundation

struct Message: Codable {
    
    var id: String!
    var roomId: String!
    var senderId: String!
    var message: String!
    var messageType: String!
    var createdAt: String!
    var dateAt: Date!
    
    //=>  1=Message, 2=Image, 3=Video, 4=PDF, 5=Call
    
    init(dictionary: [String: Any]) {
        if let strid = dictionary["id"] {
            self.id = "\(strid)"
        }
        if let roomId = dictionary["roomId"] {
            self.roomId = "\(roomId)"
        }
        if let senderId = dictionary["senderId"] {
            self.senderId = "\(senderId)"
        }
        if let message = dictionary["message"] {
            self.message = "\(message)"
        }
        if let messageType = dictionary["messageType"] {
            self.messageType = "\(messageType)"
        }
        if let createdAt = dictionary["createdAt"] {
            self.createdAt = "\(createdAt)"
            self.dateAt = "\(createdAt)".toDate()
        }
    }
    
    func getDictionary() -> [String: Any] {
        
        var msg = [String: Any]()
        
        msg["id"] = self.id
        msg["roomId"] = self.roomId
        msg["senderId"] = self.senderId
        msg["message"] = self.message
        msg["messageType"] = self.messageType
        msg["createdAt"] = self.createdAt
        msg["dateAt"] = self.dateAt
        
        return msg
    }
    
}

