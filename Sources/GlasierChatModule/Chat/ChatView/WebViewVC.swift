//
//  WebViewVC.swift
//  MyFramework
//
//  Created by iOS Developer on 06/03/23.
//  Copyright © 2023 GirAppe Studio. All rights reserved.
//

import UIKit
import WebKit

class WebViewVC: UIViewController, WKNavigationDelegate {

    //MARK: - Outside variables
    var strUrl = ""
    
    //MARK: - IBOutlet
    @IBOutlet weak var webView : WKWebView!
    
    //MARK: - Inside variables
    
    //MARK: - Inside variables
    
    //MARK: - UIView related methods
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.setWebView()
    }

    //MARK: - Setupview
    func setWebView() {
        // loading URL :
        let url = NSURL(string: self.strUrl)
            let request = NSURLRequest(url: url! as URL)

            webView.navigationDelegate = self
            webView.load(request as URLRequest)
            self.view.addSubview(webView)
    }

    //MARK:- WKNavigationDelegate

    private func webView(webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: NSError) {
        print(error.localizedDescription)
    }
    
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        print("Strat to load")
        self.view.activityStartAnimating(activityColor: UIColor.white, backgroundColor: UIColor.black.withAlphaComponent(0.5))
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        print("finish to load")
        self.view.activityStopAnimating()
    }
    
    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
