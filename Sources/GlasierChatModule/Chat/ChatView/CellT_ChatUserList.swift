//
//  CellT_ChatUserList.swift
//  OETforNurses
//
//  Created by iOS Developer on 27/02/23.
//  Copyright © 2023 MVD. All rights reserved.
//

import UIKit

class CellT_ChatUserList: UITableViewCell {
    
    //MARK: - IBOutlets
    @IBOutlet weak var imgProfilePhoto: UIImageView!
    @IBOutlet weak var imgUserStatus: UIImageView!
    @IBOutlet weak var txtUserName: UILabel!
    @IBOutlet weak var txtUserType: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.setupView()
    }
    
    //MARK: - SetupView
    func setupView() {
        self.imgProfilePhoto.layer.cornerRadius = 25
        self.imgUserStatus.layer.cornerRadius = 5
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
