//
//  Cell_CallLogForChat.swift
//  PashuCare
//
//  Created by geet on 28/06/22.
//  Copyright © 2022 Nirav Patel. All rights reserved.
//

import UIKit

class CallLogForChat: UITableViewCell {

    @IBOutlet var lblCallLog: UILabel!
    @IBOutlet var imgCall: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.imgCall.image = self.imgCall.image?.withRenderingMode(.alwaysTemplate)
        self.imgCall.tintColor = UIColor.darkGray
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
