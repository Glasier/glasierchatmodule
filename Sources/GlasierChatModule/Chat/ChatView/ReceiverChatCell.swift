//
//  ReceiverChatCell.swift
//  PashuCare
//
//  Created by geet on 08/06/22.
//  Copyright © 2022 Nirav Patel. All rights reserved.
//

import UIKit
//import SDWebImage

class ReceiverChatCell: UITableViewCell {

    @IBOutlet weak var leftUserLabel: UILabel!
    @IBOutlet weak var leftUserName: UILabel!
    @IBOutlet weak var txtTextView: UITextView!
    @IBOutlet weak var leftUserBgView: UIView!
    @IBOutlet var leftImageView: ImageLoader!
    @IBOutlet var leftImgBgView: UIView!
    @IBOutlet var ImgBgViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet var ImgBgViewRightConstraint: NSLayoutConstraint!
    @IBOutlet var audioImageView: ImageLoader!
    @IBOutlet var pdfImageView: ImageLoader!
    @IBOutlet var btnPlay: UIButton!
    @IBOutlet var imgPlay: UIImageView!
    @IBOutlet weak var mainBGView: UIView!
    
    private var isGotURL = false
    private var msgType = ""
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.imgPlay.layer.cornerRadius = 5
        self.leftImageView.layer.cornerRadius = 5
        self.txtTextView.layer.cornerRadius = 5
        self.mainBGView.layer.cornerRadius = 5
        
        self.leftUserLabel.isHidden = true
        self.leftUserBgView.isHidden = true
        self.leftImageView.isHidden = true
        self.audioImageView.isHidden = true
        self.leftImgBgView.isHidden = true
        self.btnPlay.isHidden = true
        self.imgPlay.isHidden = true
        
    }
    
    private var type: CellType = .right {
        didSet {
//            let rightHidden = type == .left ? true : false
            
//            rightUserLabel.isHidden = rightHidden
//            rightUserBgView.isHidden = rightHidden
        }
    }
    
    private var content: String? {
        didSet {
            switch type {
            case .right: break

            case .left:
                let isImageShow = self.msgType != SocketHelper.shared.TYPE_MESSAGE
                self.leftImgBgView.isHidden = !isImageShow
                self.leftUserBgView.isHidden = isImageShow
                self.ImgBgViewBottomConstraint.isActive = isImageShow
                self.ImgBgViewRightConstraint.isActive = isImageShow
                self.audioImageView.isHidden = true
                
                if isGotURL, isImageShow {
                    
                    guard let url = URL(string:content!) else { return }
                    
                    let strFileExtension = url.pathExtension
                    
                    self.leftImageView.isHidden = strFileExtension == "mp3"
                    self.audioImageView.isHidden = !(strFileExtension == "mp3")
                    self.btnPlay.isHidden = !(strFileExtension == "mp3" || strFileExtension == "mp4")
                    self.imgPlay.isHidden = !(strFileExtension == "mp3" || strFileExtension == "mp4")
                    
//                    if strFileExtension == "mp4" {
                    if self.msgType == SocketHelper.shared.TYPE_VIDEO {
                        self.leftImageView.image = NetworkManager.shared.getVideoThumbnail(url: url)
                        self.pdfImageView.isHidden = true
                    } else if self.msgType == SocketHelper.shared.TYPE_IMAGE {
//                        self.leftImageView.loadImageWithUrl(url) // call this line for getting image to yourImageView
                        self.leftImageView.image = nil
//                        NetworkManager.shared.loadImageWithUrl(url) { image in
//                            self.leftImageView.image = image
//                        }
                        NetworkManager.shared.loadImageWithUrl(url, imageView: self.leftImageView, isFromSender: true)
                        self.pdfImageView.isHidden = true
                    } else if self.msgType == SocketHelper.shared.TYPE_PDF {
                        self.pdfImageView.isHidden = false
                        self.audioImageView.isHidden = true
                        self.leftImageView.isHidden = true
                    } else if self.msgType == SocketHelper.shared.TYPE_CALL {
                        self.audioImageView.isHidden = false
                        self.leftImageView.isHidden = true
                        self.pdfImageView.isHidden = true
                    }
                } else {
                    self.leftImgBgView.isHidden = true
                    self.txtTextView.text = content
                    self.txtTextView.linkTextAttributes = [.foregroundColor: UIColor.systemYellow]
                    self.txtTextView.isSelectable = true
                    self.txtTextView.isEditable = false
                    self.txtTextView.isUserInteractionEnabled = true
                    self.txtTextView.dataDetectorTypes = .link
                    self.leftUserLabel.isHidden = true
                }
            }
        }
    }
    
    func update(type: CellType, message: Message,isGotURL:Bool, msgType: String) {
        self.isGotURL = isGotURL
        self.type = type
        self.msgType = msgType
        self.leftUserName.text = message.senderId
        self.content = message.message
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
