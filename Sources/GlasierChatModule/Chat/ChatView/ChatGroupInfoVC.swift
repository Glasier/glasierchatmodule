//
//  ChatGroupInfoVC.swift
//  OETforNurses
//
//  Created by iOS Developer on 27/02/23.
//  Copyright © 2023 MVD. All rights reserved.
//

import UIKit

class ChatGroupInfoVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    //MARK: - Outside variables
    var strGroupName = ""
    var strRoomId = ""
    var strUserID = ""
    var strtype = ""
    
    //MARK: - Inside variables
    var arrUserList = [Model_ChatUserList]()
    
    //MARK: - IBOutlets
    @IBOutlet weak var tblUsertList: UITableView!
    @IBOutlet var lblGroupName: UILabel!
    @IBOutlet var lblGroupParticipants: UILabel!

    //MARK: - UIView Related methods
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setupView()
    }

    //MARK: - SetupView
    func setupView() {
        
        self.tblUsertList.delegate = self
        self.tblUsertList.dataSource = self
        
        self.tblUsertList.register(UINib(nibName: "CellT_ChatUserList", bundle: Bundle.module), forCellReuseIdentifier: "CellT_ChatUserList")
        
        self.lblGroupName.text = self.strGroupName
        
    }

    override func viewWillAppear(_ animated: Bool) {
        self.view.activityStartAnimating(activityColor: UIColor.white, backgroundColor: UIColor.black.withAlphaComponent(0.5))
        
        SocketHelper.shared.joinChatRoom(roomId: self.strRoomId, userId: self.strUserID, userType: self.strtype, completion: {
            SocketHelper.shared.getUserList { userInfo in
                self.view.activityStopAnimating()
                print(userInfo![0].getDictionary())
                
                
                guard let arrOfUserInfo = userInfo else {
                    return
                }
                
                self.arrUserList = arrOfUserInfo
                
                if let i = arrOfUserInfo.firstIndex(where: { $0.struser_id == objUserInfo.strid }) {
                    arrOfUserInfo[i].struser_name = arrOfUserInfo[i].struser_name + " (You)"
                    
                    self.arrUserList.remove(at: i)
                    self.arrUserList.insert(arrOfUserInfo[i], at: 0)
                }
                
                self.tblUsertList.reloadData()
                
                self.lblGroupParticipants.text = "\(self.arrUserList.count) participants"
            }
        })
        
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - IBActions
    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: - UITableview delegate and datasourse methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrUserList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "CellT_ChatUserList", for: indexPath) as? CellT_ChatUserList {
            
            let objCell = self.arrUserList[indexPath.row]
            
            cell.txtUserName.text = objCell.struser_name
            cell.txtUserType.text = objCell.strtype
            
            cell.imgUserStatus.isHidden = objCell.strstatus == "1" ? false : true
            
            return cell
        }
        
        return UITableViewCell()
    }
}
