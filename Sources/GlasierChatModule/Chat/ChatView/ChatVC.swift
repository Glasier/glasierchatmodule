//
//  ChatVC.swift
//  PashuCare
//
//  Created by geet on 19/05/22.
//  Copyright © 2022 Nirav Patel. All rights reserved.
//

import UIKit
import MobileCoreServices
import QuickLook
import PDFKit
import AVFoundation
import AVKit
import JitsiMeetSDK

enum ChatType {
    case peer(String), group(String)
    
    var description: String {
        switch self {
        case .peer:  return "peer"
        case .group: return "channel"
        }
    }
}

enum fileType {
    case PDF, Video, Photo, Audio
}


class ChatVC: UIViewController, UITableViewDataSource, UITableViewDelegate, UITextViewDelegate , UIDocumentPickerDelegate,QLPreviewControllerDataSource, QLPreviewControllerDelegate, JitsiMeetViewDelegate {

    //MARK: - IBOutlets
    @IBOutlet var imgViewUser: UIImageView!
    @IBOutlet var lblUserName: UILabel!
    @IBOutlet var lblUserStatus: UILabel!
    @IBOutlet var tblViewChat: UITableView!
    @IBOutlet var txtMessage: UITextView!
    @IBOutlet var btnBack: UIButton!
    @IBOutlet var btnAudioCall: UIButton!
    @IBOutlet var btnSend: UIButton!
    @IBOutlet var btnAttachImage: UIButton!
    @IBOutlet var viewMessageTypeing: UIView!
    @IBOutlet weak var imgBack: UIImageView!
    @IBOutlet weak var imgBG: UIImageView!
    
    //MARK: - Variables
    var strUserName = ""
    var strUserImg = ""
    var strTitle = ""
    var placeholderLabel : UILabel!
    var SelectFileType: fileType = .Photo
    var isSelectPDF = false
    var UploadImg:UIImage?
    var UploadFileURL:URL?
    var previewPDF:URL?
    
    //For Notification
    var isFromNotification = false
    var isLoadOldMessages = true
    
    //For VoiceCall
    var strUserID = ""
    var strAcceptedUserID = ""
    var strAcceptedUserChanel = ""
    
    lazy var msgList = [Date:[Message]]()
    var isfromDr = false
    var dateArray: [Date] = []
    
    //MARK: - New for socketio
    var strRoomId = ""
    private var messageViewModel: MessageViewModel = MessageViewModel()
    var kRequestType = false
    private lazy var photoPicker = PhotoPicker()
    
    var arrRoomList = Model_RoomList()
    var objUserType = Model_UserType()
    
    fileprivate var pipViewCoordinator: PiPViewCoordinator?
    fileprivate var jitsiMeetView: JitsiMeetView?
    
    //MARK: - UIView related methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.updateViews()
        
        self.title = ""
        self.navigationItem.setHidesBackButton(true, animated: false)
        
        if isFromNotification {
            //            CCSLoadingView().displayLoadingView()
            _ = Timer.scheduledTimer(withTimeInterval: 10, repeats: false, block: { timer in
                //                CCSLoadingView().removeLoadingView()
                SocketHelper.shared.establishConnection()
                self.joinChatWithRecieverId(channelName: "inc")
            })
        } else {
            
        }
        
        self.imgBack.image = SCImage(named: "ic_Back")
        self.imgBG.image = SCImage(named: "ic_AppBG")
        self.btnAudioCall.setImage(SCImage(named: "ic_AudioCall"), for: .normal)
        
        //        AppDelegate.getInstance.strChatRoomId = self.strRoomId
        //        AppDelegate.getInstance.isPresentChatNotification = false
        
        self.viewMessageTypeing.isHidden = self.kRequestType
        self.btnAudioCall.isHidden = self.kRequestType
        
        let nc = NotificationCenter.default
        nc.addObserver(self, selector: #selector(appMovedToForeground), name: UIApplication.willEnterForegroundNotification, object: nil)
        
        self.view.activityStartAnimating(activityColor: UIColor.white, backgroundColor: UIColor.black.withAlphaComponent(0.5))
        SocketHelper.shared.establishConnection()
        _ = Timer.scheduledTimer(withTimeInterval: 5, repeats: false, block: { timer in
            self.view.activityStopAnimating()
            self.joinChatWithRecieverId(channelName: "inc")
        })
        
//        self.setupJitsi()
        
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc func appMovedToForeground() {
        SocketHelper.shared.establishConnection()
    }
    
//    func setupJitsi() {
//        let defaultOptions = JitsiMeetConferenceOptions.fromBuilder { (builder) in
//            // for JaaS replace url with https://8x8.vc
////            builder.serverURL = URL(string: "https://meet.jit.si")
//            builder.serverURL = URL(string: "https://jitsicall.online/")
//
////            builder.serverURL = URL(string: "https://jitsi.glasierinc.in/")
//
//            // for JaaS use the obtained Jitsi JWT
//            // builder.token = "SampleJWT"
//            builder.setFeatureFlag("welcomepage.enabled", withValue: false)
//            // Set different feature flags
//            builder.setFeatureFlag("toolbox.enabled", withBoolean: false)
//            builder.setFeatureFlag("filmstrip.enabled", withBoolean: false)
//            builder.setFeatureFlag("chat.enabled", withBoolean: false)
//            builder.setFeatureFlag("settings.enabled", withBoolean: false)
//            builder.setFeatureFlag("toolbox.enabled", withBoolean: false)
//            
//            //For more flags :  https://jitsi.github.io/handbook/docs/dev-guide/mobile-feature-flags/
//        }
//
//        JitsiMeet.sharedInstance().defaultConferenceOptions = defaultOptions
//    }

    override func viewWillTransition(to size: CGSize,
                                     with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)

        let rect = CGRect(origin: CGPoint.zero, size: size)
        // Reset view to provide bounds. It is required to do so
        // on rotation or screen size changes.
        pipViewCoordinator?.resetBounds(bounds: rect)
    }

    fileprivate func cleanUp() {
        jitsiMeetView?.removeFromSuperview()
        jitsiMeetView = nil
        pipViewCoordinator = nil
    }

    func ready(toClose data: [AnyHashable : Any]!) {
        self.pipViewCoordinator?.hide() { _ in
            self.cleanUp()
        }
    }

    func enterPicture(inPicture data: [AnyHashable : Any]!) {
        self.pipViewCoordinator?.enterPictureInPicture()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        lblUserName.text = strTitle
        self.lblUserStatus.text = "Tab to show group info."
        //        imgViewUser.kf.setImage(with: URL(string: strUserImg), placeholder: UIImage(named: "imgPlaceholderUser"))
        
        self.navigationController?.navigationBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        //        AppDelegate.getInstance.strChatRoomId = ""
        //        AppDelegate.getInstance.isPresentChatNotification = true
        self.navigationController?.navigationBar.isHidden = false
    }
    
    //MARK: - New code for socketio
    func joinChatWithRecieverId(channelName: String) {
        self.strUserID = self.objUserType.struser_id
        self.strRoomId = self.arrRoomList.strroom_id
        SocketHelper.shared.joinChatRoom(roomId: self.strRoomId, userId: self.strUserID, userType: self.objUserType.strtype, completion: {
            self.getMessage()
        })
    }
    
    func getMessage() {
        
        self.messageViewModel.arrMessage.subscribe { [weak self] (result: [Message]) in
            
            guard let self = self else {
                return
            }
            
            self.tblViewChat.reloadData()
        }
        
        SocketHelper.shared.getMessageHistory { messageInfo in
            print(messageInfo)
            self.messageViewModel.arrMessage.value = messageInfo
            
//            let grouped = messageInfo.sliced(by: [.day], for: \.dateAt)
//            self.msgList = grouped
//            print(grouped)
//
//            self.dateArray = Array(self.msgList.keys).sorted(by: >)
            
            self.tblViewChat.reloadData()
            self.scrollToBottom(isAnimated: false)
            
        }
        
        SocketHelper.shared.getMessage { [weak self] (message: Message?) in
            
            guard let self = self,
                  let msgInfo = message else {
                return
            }
            
            self.messageViewModel.arrMessage.value.append(msgInfo)
            self.scrollToBottom(isAnimated: true)
            self.tblViewChat.reloadData()
        }
    }
    
    func scrollToBottom(isAnimated: Bool) {
        
        let msgCount = self.messageViewModel.arrMessage.value.count
        
        if msgCount > 1 {
            
            let end = IndexPath(row: msgCount - 1, section: 0)
            self.tblViewChat.scrollToRow(at: end, at: .bottom, animated: isAnimated)
        }
    }
    
    //TODO: - FileUpload API
    private func CallwebServiceFileUpload(data: Data?) {

        var imgData:Data?
        if SelectFileType == .Photo {
            imgData = UploadImg?.jpegData(compressionQuality: 0.3)!
        } else if let data1 = data {
            imgData = data1
        }
        
        var imageFilename = ""
        var imageMimeType = ""
        var fileType = ""
        
        switch SelectFileType {
        case .PDF:
            imageFilename = "Uploadpdf.pdf"
            imageMimeType = "application/pdf"
            fileType = SocketHelper.shared.TYPE_PDF
        case .Video:
            imageFilename = "UploadVideo.mp4"
            imageMimeType = "video/*"
            fileType = SocketHelper.shared.TYPE_VIDEO
        case .Audio:
            imageFilename = "UploadAudio.mp3"
            imageMimeType = "audio/*"
            fileType = SocketHelper.shared.TYPE_CALL
        default:
            imageFilename = "Uploadimage.jpg"
            imageMimeType = "image/jpg"
            fileType = SocketHelper.shared.TYPE_IMAGE
        }
        
        let params = ["room_id":self.arrRoomList.strroom_id,
                      "user_id":self.objUserType.struser_id,
                      "type":self.objUserType.strtype,
                      "file_type":fileType]
        
        NetworkManager.shared.showProgressView(in: self.view)
        
        NetworkManager.shared.uploadData(urlString: API.Upload_attach_file, parameters: params, imageFilename: imageFilename, imageKeyName: "attach_file", imageMimeType: imageMimeType, imageData: imgData!) { status, result in
            NetworkManager.shared.hideProgressView()
            if status {
                let messageType = self.SelectFileType == .Photo ? SocketHelper.shared.TYPE_IMAGE : self.SelectFileType == .Video ? SocketHelper.shared.TYPE_VIDEO : self.SelectFileType == .PDF ? SocketHelper.shared.TYPE_PDF : self.SelectFileType == .Audio ? SocketHelper.shared.TYPE_CALL : SocketHelper.shared.TYPE_MESSAGE
                
                if let objDict = result as? NSDictionary {
                    if let objMsg = objDict.value(forKey: "message_list") as? NSDictionary, let strUrl = objMsg.value(forKey: "attach_file") as? String {
                        self.send(message: strUrl, messageType: messageType)
                    }
                }
            }
        }
    }
    
    //MARK: - CustomMethods
    func updateViews() {
        self.btnBack.setTitle("", for: .normal)
        self.btnAudioCall.setTitle("", for: .normal)
        self.btnSend.setTitle("", for: .normal)
        
        self.btnBack.imageView?.contentMode = .scaleAspectFit
        self.btnAudioCall.imageView?.contentMode = .scaleAspectFit
        
        self.tblViewChat.rowHeight = UITableView.automaticDimension
        self.tblViewChat.estimatedRowHeight = 55
        
        self.txtMessage.delegate = self
        self.placeholderLabel = UILabel()
        self.placeholderLabel.text = "Type a message..."
        self.placeholderLabel.font = .italicSystemFont(ofSize: (txtMessage.font?.pointSize)!)
        self.placeholderLabel.sizeToFit()
        self.txtMessage.addSubview(placeholderLabel)
        self.placeholderLabel.frame.origin = CGPoint(x: 5, y: (txtMessage.font?.pointSize)! / 2)
        self.placeholderLabel.textColor = .lightGray
        self.placeholderLabel.isHidden = !txtMessage.text.isEmpty
        
        self.viewMessageTypeing.layer.borderColor = UIColor.darkGray.cgColor
        self.viewMessageTypeing.layer.borderWidth = 1
        self.viewMessageTypeing.layer.cornerRadius = 10
        
        self.txtMessage.keyboardType = .asciiCapable
        self.tblViewChat.delegate = self
        self.tblViewChat.dataSource = self
        
    }
    
    //MARK: - UITextView Delegate Methods
    func textViewDidChange(_ textView: UITextView) {
        placeholderLabel.isHidden = !textView.text.isEmpty
    }
    
    //MARK: - TableviewDataSource
//    func numberOfSections(in tableView: UITableView) -> Int {
//        return self.dateArray.count
//    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.messageViewModel.arrMessage.value.count
//        return self.msgList[self.dateArray[section]]!.count
    }
//    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
//        return self.dateArray[section].formatRelativeString()
//    }
    //ReceiverChatCell
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let msg = self.messageViewModel.arrMessage.value[indexPath.row]
//        let msgSection = self.msgList[self.dateArray[indexPath.section]]
//        let msg = msgSection![indexPath.row]
        let type: CellType = msg.senderId == self.strUserID ? .right : .left
        
        if type == .left {
//            tableView.register(UINib(nibName: "ReceiverChatCell", bundle: nil), forCellReuseIdentifier: "ReceiverChatCell")
            self.tblViewChat.register(UINib(nibName: "ReceiverChatCell", bundle: Bundle.module), forCellReuseIdentifier: "ReceiverChatCell")
            let cell = tableView.dequeueReusableCell(withIdentifier: "ReceiverChatCell", for: indexPath) as! ReceiverChatCell
            
            cell.btnPlay.addTarget(self, action: #selector(self.openPlayer(sender:)), for: .touchUpInside)
            cell.btnPlay.tag = indexPath.row
            
            cell.update(type: type, message: msg, isGotURL: ifFoundURL(input: msg.message), msgType: msg.messageType)
            return cell
            
        } else {
//            tableView.register(UINib(nibName: "ChatCell", bundle: nil), forCellReuseIdentifier: "ChatCell")
            self.tblViewChat.register(UINib(nibName: "ChatCell", bundle: Bundle.module), forCellReuseIdentifier: "ChatCell")
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "ChatCell", for: indexPath) as! ChatCell
            
            cell.btnPlay.addTarget(self, action: #selector(self.openPlayer(sender:)), for: .touchUpInside)
            cell.btnPlay.tag = indexPath.row
            
//            cell.msgDate.text = msg.createdAt.toDate()?.formatRelativeString()
            
            cell.update(type: type, message: msg, isGotURL: ifFoundURL(input: msg.message), msgType: msg.messageType)
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let msg = self.messageViewModel.arrMessage.value[indexPath.row]
        if msg.messageType != SocketHelper.shared.TYPE_MESSAGE {
            if ifFoundURL(input: msg.message) {
                if let url = URL(string:msg.message), url.pathExtension != "mp4", url.pathExtension != "mp3" {
                    let objWebViewVC = WebViewVC(nibName: "WebViewVC", bundle:
                                                    Bundle.module)
                    objWebViewVC.strUrl = msg.message
                    self.navigationController?.pushViewController(objWebViewVC, animated: true)
                }
            }
        }
    }
    
    @objc func openPlayer(sender: UIButton) {
        
        let msg = self.messageViewModel.arrMessage.value[sender.tag]
        if msg.messageType != SocketHelper.shared.TYPE_MESSAGE {
            if ifFoundURL(input: msg.message) {
                if let url = URL(string:msg.message),(url.pathExtension == "mp4" || url.pathExtension == "mp3") {
                    let player = AVPlayer(url: url)
                    let playerViewController = AVPlayerViewController()
                    playerViewController.player = player
                    self.present(playerViewController, animated: true) {
                        playerViewController.player!.play()
                    }
                }
            }
        }
    }
    
    func ifFoundURL(input:String) -> Bool {
        
        let detector = try! NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue)
        let matches = detector.matches(in: input, options: [], range: NSRange(location: 0, length: input.utf16.count))
        
        for match in matches {
            guard let range = Range(match.range, in: input) else { continue }
            let url = input[range]
            print("Got url:\(url)")
            return true
        }
        
        return false
    }
    
    //MARK: - SendMessage
    func send(message: String, messageType: String) {
        self.strAcceptedUserChanel = "inc"
        txtMessage.resignFirstResponder()
        SocketHelper.shared.sendMessage(roomId: self.strRoomId, userId: self.strUserID, message: message, messageType: messageType, channelName:self.strAcceptedUserChanel, token:"")
        self.placeholderLabel.isHidden = false
        txtMessage.text = nil
    }
    
    //MARK: - ImagePickerDelegate
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        picker.dismiss(animated: true, completion: nil)
        
        if let videoUrl = info[UIImagePickerController.InfoKey.mediaURL] as? URL {
            encodeVideo(videoUrl)
        } else {
            let image = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
            self.UploadImg = image
            self.CallwebServiceFileUpload(data: nil)
        }
    }
    
    func encodeVideo(_ videoURL: URL)  {
        let avAsset = AVURLAsset(url: videoURL, options: nil)
        
        //Create Export session
        guard let exportSession = AVAssetExportSession(asset: avAsset, presetName: AVAssetExportPresetLowQuality) else {
            
            return
        }
        
        let filePath = NSURL.fileURL(withPath: NSTemporaryDirectory() + NSUUID().uuidString + ".mp4")
        
        
        exportSession.outputURL = filePath
        exportSession.outputFileType = AVFileType.mp4
        exportSession.shouldOptimizeForNetworkUse = true
        
        exportSession.exportAsynchronously(completionHandler: {() -> Void in
            DispatchQueue.main.async {
                
                switch exportSession.status {
                case .failed:
                    print("Failed:\(exportSession.error?.localizedDescription ?? "")")
                case .cancelled:
                    print("Export canceled")
                case .completed:
                    if let url = exportSession.outputURL {
                        self.UploadFileURL = url
                        //                        self.CallwebServiceFileUpload()
                    }
                default:
                    break
                }
            }
        })
    }
    
    //MARK: - documentPickerMethods
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        
        guard let pdfUrl = urls.first else {
            return
        }
        print("url:\(pdfUrl)")
        
        // Create file URL to temporary folder
        var tempURL = URL(fileURLWithPath: NSTemporaryDirectory())
        // Apend filename (name+extension) to URL
        tempURL.appendPathComponent(pdfUrl.lastPathComponent)
        do {
            // If file with same name exists remove it (replace file with new one)
            if FileManager.default.fileExists(atPath: tempURL.path) {
                try FileManager.default.removeItem(atPath: tempURL.path)
            }
            // Move file from app_id-Inbox to tmp/filename
            try FileManager.default.moveItem(atPath: pdfUrl.path, toPath: tempURL.path)
            self.UploadFileURL =  tempURL
            
            do {
                let imageData = try Data(contentsOf: tempURL as URL)
                self.CallwebServiceFileUpload(data: imageData)
            } catch {
                print("Unable to load data: \(error)")
            }
            
        } catch {
            print(error.localizedDescription)
        }
        
    }
    
    func numberOfPreviewItems(in controller: QLPreviewController) -> Int {
        self.previewPDF == nil ? 0 : 1
    }
    
    func previewController(_ controller: QLPreviewController, previewItemAt index: Int) -> QLPreviewItem {
        return self.previewPDF! as QLPreviewItem
    }
    
    func previewControllerWillDismiss(_ controller: QLPreviewController) {
        self.previewPDF = nil
    }
    
    //MARK: - ButtonActions
    @IBAction func btnBackAction(_ sender: Any) {
        self.txtMessage.endEditing(true)
        
        if self.isFromNotification {
            //            AppDelegate.getInstance.methodToLoadRootViewController(initial: false)
        } else {
            self.navigationController?.popViewController(animated: true)
        }
        SocketHelper.shared.closeConnection()
    }
    
    @IBAction func btnAttachImageAction(_ sender: Any) {
        self.txtMessage.endEditing(true)
        
        let actionsheet = UIAlertController(title: "", message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        
        actionsheet.addAction(UIAlertAction(title: "TAKE PHOTO", style: UIAlertAction.Style.default, handler: { (action) -> Void in
            
            self.SelectFileType = .Photo
            self.photoPicker.pick(allowsEditing: false,
                                  pickerSourceType: .Camera,controller: self) { originalPhoto, editedPhoto, strUrl  in
                print("originalPhoto")
                self.UploadImg = originalPhoto
                self.CallwebServiceFileUpload(data: nil)
            }
            
        }))
        
        actionsheet.addAction(UIAlertAction(title: "SELEC FROM GALLARY", style: UIAlertAction.Style.default, handler: { (action) -> Void in
            
            self.SelectFileType = .Photo
            self.photoPicker.pick(controller: self) { originalPhoto, editedPhoto, strUrl in
                print("originalPhoto")
                
                self.UploadImg = originalPhoto
                self.CallwebServiceFileUpload(data: nil)
            }
        }))
        actionsheet.addAction(UIAlertAction(title: "VIDEOS", style: UIAlertAction.Style.default, handler: { (action) -> Void in
            
            self.SelectFileType = .Video
            self.photoPicker.pick(pickerSourceType: .Video, controller: self) { originalPhoto, editedPhoto, strUrl in
                
                if let fileUrl = URL(string: strUrl!) {
                    do {
                        let imageData = try Data(contentsOf: fileUrl as URL)
                        self.CallwebServiceFileUpload(data: imageData)
                    } catch {
                        print("Unable to load data: \(error)")
                    }
                }
            }
            
        }))
        
        actionsheet.addAction(UIAlertAction(title: "AUDIO", style: UIAlertAction.Style.default, handler: { (action) -> Void in
            
            self.SelectFileType = .Audio
            let documentPicker = UIDocumentPickerViewController(documentTypes: [String(kUTTypeAudio)], in: .import)
            documentPicker.delegate = self
            self.present(documentPicker, animated: true)
            
        }))
        
        actionsheet.addAction(UIAlertAction(title: "PDF", style: UIAlertAction.Style.default, handler: { (action) -> Void in
            
            self.SelectFileType = .PDF
            let documentPicker = UIDocumentPickerViewController(documentTypes: [String(kUTTypePDF)], in: .import)
            documentPicker.delegate = self
            self.present(documentPicker, animated: true)
        }))
        
        actionsheet.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: { (action) -> Void in
            
        }))
        self.present(actionsheet, animated: true, completion: nil)
    }
    
    @IBAction func btnChatSendAction(_ sender: Any) {
        self.txtMessage.endEditing(true)
        
        guard let text = txtMessage.text,text.trimmingCharacters(in: .whitespaces).count > 0 else{
            self.txtMessage.text = nil
            return
        }
        txtMessage.endEditing(true)
        send(message: text, messageType: SocketHelper.shared.TYPE_MESSAGE)
    }
    
    @IBAction func btnAudioCallAction(_ sender: Any) {
        self.txtMessage.endEditing(true)

        // create and configure jitsimeet view
        let jitsiMeetView = JitsiMeetView()
        jitsiMeetView.delegate = self
    
        self.jitsiMeetView = jitsiMeetView
        let options = JitsiMeetConferenceOptions.fromBuilder { (builder) in
            // for JaaS use <tenant>/<roomName> format
            
            builder.room = self.strRoomId
            let objUser = MyPublicClass()
            if strJitsiURL == "" {
                print("Set Jitsi URL...")
            }
            builder.serverURL = URL(string: strJitsiURL)
//            builder.userInfo?.displayName = "objUser.userInfo.straccount_name"
//            builder.userInfo?.email = "objUser.userInfo.straccount_name"
            builder.userInfo = JitsiMeetUserInfo(displayName: objUserInfo.straccount_name, andEmail: objUserInfo.stremail, andAvatar: nil)

            // Settings for audio and video
            // builder.audioMuted = true;
            // builder.videoMuted = true;
            
            builder.setFeatureFlag("welcomepage.enabled", withBoolean: false)
            builder.setFeatureFlag("prejoinpage.enabled", withBoolean: false)
            builder.setFeatureFlag("meeting-name.enabled", withBoolean: false)
            
            // Set different feature flags
//            builder.setFeatureFlag("toolbox.enabled", withBoolean: false)
//            builder.setFeatureFlag("filmstrip.enabled", withBoolean: false)
            builder.setFeatureFlag("invite.enabled", withBoolean: false)
            builder.setFeatureFlag("overflow-menu.enabled", withBoolean: false)
            builder.setFeatureFlag("chat.enabled", withBoolean: false)
            builder.setFeatureFlag("settings.enabled", withBoolean: false)
//            tile-view.enabled
            
        }

        // join room and display jitsi-call
        jitsiMeetView.join(options)

        // Enable jitsimeet view to be a view that can be displayed
        // on top of all the things, and let the coordinator to manage
        // the view state and interactions
        pipViewCoordinator = PiPViewCoordinator(withView: jitsiMeetView)
        pipViewCoordinator?.configureAsStickyView(withParentView: view)

        // animate in
        jitsiMeetView.alpha = 0
        pipViewCoordinator?.show()
        
    }
    
    @IBAction func btnGroupInfo(sender: UIButton) {
//        let vc = ChatGroupInfoVC()
//
//        vc.strRoomId = self.strRoomId
//        vc.strUserID = self.strUserID
//        vc.strtype = self.objUserType.strtype
//
//        vc.strGroupName = self.strTitle
//        self.navigationController?.pushViewController(vc, animated: true)
        
        let objChatGroupInfoVC = ChatGroupInfoVC(nibName: "ChatGroupInfoVC", bundle:
                                                    Bundle.module)

        objChatGroupInfoVC.strRoomId = self.strRoomId
        objChatGroupInfoVC.strUserID = self.strUserID
        objChatGroupInfoVC.strtype = self.objUserType.strtype
        
        objChatGroupInfoVC.strGroupName = self.strTitle
        self.navigationController?.pushViewController(objChatGroupInfoVC, animated: true)
    }
    
}
