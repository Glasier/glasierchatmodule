//
//  ChatCell.swift
//  PashuCare
//
//  Created by geet on 19/05/22.
//  Copyright © 2022 Nirav Patel. All rights reserved.
//

import UIKit
import AVFoundation
//import SDWebImage

enum CellType {
    case left, right
}

class ChatCell: UITableViewCell {
    
    @IBOutlet weak var rightUserLabel: UILabel!
    @IBOutlet weak var rightUserBgView: UIView!
    @IBOutlet var rightImageView: ImageLoader!
    @IBOutlet var audioImageView: ImageLoader!
    @IBOutlet var pdfImageView: ImageLoader!
    @IBOutlet var RightImgBgView: UIView!
    @IBOutlet var ImgBgViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet var btnPlay: UIButton!
    @IBOutlet var imgPlay: UIImageView!
    @IBOutlet weak var msgDate: UILabel!
    
    @IBOutlet weak var txtTextView: UITextView!
    private var msgType = ""
    let activityIndicator = UIActivityIndicatorView()
        
    private var isGotURL = false
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.imgPlay.layer.cornerRadius = 5
        self.rightImageView.layer.cornerRadius = 5
        self.txtTextView.layer.cornerRadius = 5
        
        self.rightUserLabel.isHidden = true
        self.rightUserBgView.isHidden = true
        self.rightImageView.isHidden = true
        self.audioImageView.isHidden = true
        self.RightImgBgView.isHidden = true
        self.btnPlay.isHidden = true
        self.imgPlay.isHidden = true
        
        
        // setup activityIndicator...
        if #available(iOS 13.0, *) {
            activityIndicator.color = .systemGray6
        } else {
            activityIndicator.color = .darkGray
        }
        self.activityIndicator.frame = CGRect(x: 0, y: 0, width: 46, height: 46)
        self.rightImageView.addSubview(activityIndicator)
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        activityIndicator.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        activityIndicator.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        activityIndicator.backgroundColor = .red
    }
    
    private var type: CellType = .right {
        didSet {
//            let rightHidden = type == .left ? true : false
            
//            rightUserLabel.isHidden = rightHidden
//            rightUserBgView.isHidden = rightHidden
        }
    }
    
    private var content: String? {
        didSet {
            switch type {
            case .left: break

            case .right:
                let isImageShow = self.msgType != SocketHelper.shared.TYPE_MESSAGE
                self.RightImgBgView.isHidden = !isImageShow
                self.rightUserBgView.isHidden = isImageShow
                self.ImgBgViewBottomConstraint.isActive = isImageShow
                self.audioImageView.isHidden = true
                
                if isGotURL, isImageShow {
                    
                    guard let url = URL(string:content!) else { return }
                    
                    let strFileExtension = url.pathExtension
                    
                    self.rightImageView.isHidden = strFileExtension == "mp3"
                    self.audioImageView.isHidden = !(strFileExtension == "mp3")
                    self.btnPlay.isHidden = !(strFileExtension == "mp3" || strFileExtension == "mp4")
                    self.imgPlay.isHidden = !(strFileExtension == "mp3" || strFileExtension == "mp4")
                    
//                    if strFileExtension == "mp4" {
                    if self.msgType == SocketHelper.shared.TYPE_VIDEO {
                        self.rightImageView.image = NetworkManager.shared.getVideoThumbnail(url: url)
                        self.pdfImageView.isHidden = true
                    } else if self.msgType == SocketHelper.shared.TYPE_IMAGE {
//                        self.rightImageView.loadImageWithUrl(url) // call this line for getting image to yourImageView
                        self.rightImageView.image = nil
                        self.activityIndicator.startAnimating()
//                        NetworkManager.shared.loadImageWithUrl(url) { image in
//                            self.activityIndicator.stopAnimating()
//                            self.rightImageView.image = image
//                        }
                        NetworkManager.shared.loadImageWithUrl(url, imageView: self.rightImageView, isFromSender: false)
                        self.pdfImageView.isHidden = true
                    } else if self.msgType == SocketHelper.shared.TYPE_PDF {
                        self.pdfImageView.isHidden = false
                        self.audioImageView.isHidden = true
                        self.rightImageView.isHidden = true
                    } else if self.msgType == SocketHelper.shared.TYPE_CALL {
                        self.audioImageView.isHidden = false
                        self.rightImageView.isHidden = true
                        self.pdfImageView.isHidden = true
                    }
                } else {
                    self.RightImgBgView.isHidden = true
                    self.txtTextView.text = content
                    self.txtTextView.linkTextAttributes = [.foregroundColor: UIColor.systemYellow]
                    self.txtTextView.isSelectable = true
                    self.txtTextView.isEditable = false
                    self.txtTextView.isUserInteractionEnabled = true
                    self.txtTextView.dataDetectorTypes = .link
                    self.rightUserLabel.isHidden = true
                }
            }
        }
    }
    
    func update(type: CellType, message: Message,isGotURL:Bool, msgType: String) {
        self.isGotURL = isGotURL
        self.type = type
        self.msgType = msgType
        self.content = message.message
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}

extension AVAsset {

    func generateThumbnail(completion: @escaping (UIImage?) -> Void) {
        DispatchQueue.global().async {
            let imageGenerator = AVAssetImageGenerator(asset: self)
            let time = CMTime(seconds: 0.0, preferredTimescale: 600)
            let times = [NSValue(time: time)]
            imageGenerator.generateCGImagesAsynchronously(forTimes: times, completionHandler: { _, image, _, _, _ in
                if let image = image {
                    completion(UIImage(cgImage: image))
                } else {
                    completion(nil)
                }
            })
        }
    }
}
