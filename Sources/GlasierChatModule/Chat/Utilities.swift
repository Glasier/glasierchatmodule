//
//  Utilities.swift
//  MyFramework
//
//  Created by iOS Developer on 28/02/23.
//  Copyright © 2023 GirAppe Studio. All rights reserved.
//

import UIKit
import Foundation

extension UIView {
    
    func activityStartAnimating(activityColor: UIColor, backgroundColor: UIColor) {
        DispatchQueue.main.async {
            let backgroundView = UIView()
            backgroundView.frame = CGRect.init(x: 0, y: 0, width: self.bounds.width, height: self.bounds.height)
            backgroundView.backgroundColor = backgroundColor
            backgroundView.tag = 475647
            
            var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
            activityIndicator = UIActivityIndicatorView(frame: CGRect.init(x: 0, y: 0, width: 50, height: 50))
            activityIndicator.center = self.center
            activityIndicator.hidesWhenStopped = true
            activityIndicator.style = UIActivityIndicatorView.Style.gray
            activityIndicator.color = activityColor
            activityIndicator.startAnimating()
            self.isUserInteractionEnabled = false
            
            backgroundView.addSubview(activityIndicator)
            
            self.addSubview(backgroundView)
        }
    }
    
    func activityStopAnimating() {
        DispatchQueue.main.async {
            if let background = self.viewWithTag(475647){
                background.removeFromSuperview()
            }
            self.isUserInteractionEnabled = true
        }
    }
}

class MDNavView: UIView {
    
    @IBOutlet var gvBG: MDGradientView?
    @IBOutlet var statusHeight: NSLayoutConstraint?
    
    override func draw(_ rect: CGRect) {
        // Drawing code
        super.draw(rect)
        statusHeight?.constant = Screen.IS_IPHONE_Xs_Max() || Screen.IS_IPHONE_X()  ? 45 : 20
        let objPublicClass = MyPublicClass()
        self.gvBG?.topColor = objPublicClass.topColor
        self.gvBG?.bottomColor = objPublicClass.bottomColor
    }
    
    override func awakeFromNib() {
        super .awakeFromNib()
    }
    
    
    
}

class Screen: NSObject {
    
    class func SCREEN_SIZE () -> CGSize{
        return UIScreen.main.bounds.size
    }
    
    class func SCREEN_WIDTH () -> CGFloat{
        return UIScreen.main.bounds.size.width
    }
    
    class func SCREEN_HEIGHT () -> CGFloat{
        return UIScreen.main.bounds.size.height
    }
    
    class func SCREEN_MAX_LENGTH () -> CGFloat{
        return max(Screen.SCREEN_WIDTH(), Screen.SCREEN_HEIGHT())
    }
    
    class func SCREEN_MIN_LENGTH () -> CGFloat{
        return  min(Screen.SCREEN_WIDTH(), Screen.SCREEN_HEIGHT())
    }
    
    class func WIDTHFORPER (per: CGFloat) -> CGFloat{
        return UIScreen.main.bounds.size.width * per / 100.0
    }
    
    class func HEIGHTFORPER (per: CGFloat) -> CGFloat{
        return UIScreen.main.bounds.size.height * per / 100.0
    }
    
    class func SCREEN_CENTER () -> CGPoint{
        return CGPoint(x: Screen.SCREEN_WIDTH()/2 , y: Screen.SCREEN_HEIGHT()/2 )
    }
    
    class func SCREEN_CENTER_PER ( w: CGFloat , h: CGFloat) -> CGPoint{
        return CGPoint(x: Screen.WIDTHFORPER(per: w) , y: Screen.HEIGHTFORPER(per: h))
    }
    
    class func IS_IPHONE () -> Bool{
        return UI_USER_INTERFACE_IDIOM() == .phone
    }
    
    class func IS_IPAD () -> Bool{
        return UI_USER_INTERFACE_IDIOM() == .pad
    }
    
    class func IS_IPHONE_4 () -> Bool{
        return UI_USER_INTERFACE_IDIOM() == .phone && Screen.SCREEN_MAX_LENGTH() == 480.0
    }
    
    class func IS_IPHONE_5 () -> Bool{
        return UI_USER_INTERFACE_IDIOM() == .phone && Screen.SCREEN_MAX_LENGTH() == 568.0
    }
    
    class func IS_IPHONE_6 () -> Bool{
        return UI_USER_INTERFACE_IDIOM() == .phone && Screen.SCREEN_MAX_LENGTH() == 667.0
    }
    
    class func IS_IPHONE_6P () -> Bool{
        return UI_USER_INTERFACE_IDIOM() == .phone && Screen.SCREEN_MAX_LENGTH() == 736.0
    }
    
    class func IS_IPHONE_X () -> Bool{
        return UI_USER_INTERFACE_IDIOM() == .phone && Screen.SCREEN_MAX_LENGTH() == 812.0
    }
    
    class func IS_IPHONE_Xs_Max () -> Bool{
        return UI_USER_INTERFACE_IDIOM() == .phone && Screen.SCREEN_MAX_LENGTH() == 896.0
    }
    
    class func addShadow (view : UIView, shadowOpacity: Float  , shadowRadius: CGFloat){
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowOffset = CGSize(width: 0, height: 0)
        view.layer.shadowOpacity = shadowOpacity
        view.layer.shadowRadius = shadowRadius
        view.layer.masksToBounds = false
    }
    
    class func UDSET(data: Any,key: String){
        UserDefaults.standard.set(data, forKey: key)
        UserDefaults.standard.synchronize()
    }
    
    class func UDValue(key: String)-> Any{
        return UserDefaults.standard.value(forKey:key) as Any;
    }
    
    class func UDValueBool(key: String)-> Bool{
        return UserDefaults.standard.value(forKey:key) as? Bool ?? false;
    }
    
    class func UDValueTrueBool(key: String)-> Bool{
        return UserDefaults.standard.value(forKey:key) as? Bool ?? true;
    }
    
    class func UIFontBlod(size: CGFloat)-> UIFont {
        return UIFont.init(name: "Montserrat-SemiBold", size: size) ?? UIFont.systemFont(ofSize: size)
    }
    
    class func getJson(objects:  [Any]?) -> Any?{
        
        if objects == nil {
            return nil
        }
        
        for objectsString in objects! {
            do {
                if let objectData = (objectsString as? String ?? "").data(using: .utf8) {
                    return try JSONSerialization.jsonObject(with: objectData, options: .mutableContainers)
                }
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    class func anyObjectJson(object:Any ,prettyPrint: Bool )->String {
        guard let data = try? JSONSerialization.data(withJSONObject: object, options: []) else {
            return "{}"
        }
        return String(data: data, encoding: String.Encoding.utf8)!
    }
    
    
    class func stringDateChangeFormat(data: String , format: String , new_format: String) -> String {
        let dateFormat =  DateFormatter()
        dateFormat.dateFormat = format;
        if let dt =  dateFormat.date(from: data) {
            dateFormat.dateFormat = new_format
            return dateFormat.string(from: dt)
        } else {
            return ""
        }
    }
    
    class func stringDateToNSDate(date: String , format: String) -> Date? {
        let dateFormat =  DateFormatter()
        dateFormat.dateFormat = format;
        return dateFormat.date(from: date)
    }
    
    class func nSDateToStringDate (date: Date , format: String) -> String? {
        let dateFormat =  DateFormatter()
        dateFormat.dateFormat = format;
        return dateFormat.string(from: date)
    }
    
    class func serverDateTimeToStringDate(date: String , format: String) -> String? {
        return self.stringDateChangeFormat(data: date, format: "yyyy-MM-dd'T'HH:mm:ss.SSSZ", new_format: format)
    }
    
    class func serverDateTimeToDate(date: String) -> Date? {
        return self.stringDateToNSDate(date: date, format: "yyyy-MM-dd'T'HH:mm:ss.SSSZ")
    }
    
    class func calAge(date: String) -> String {
        if let startDate = self.stringDateToNSDate(date: date,format: "dd-MM-yyyy") {
            let endDate = Date()
            let formatter = DateComponentsFormatter()
            formatter.unitsStyle = .full
            formatter.allowedUnits = [NSCalendar.Unit.year]
            return formatter.string(from: startDate, to: endDate) ?? ""
        } else {
            return ""
        }
    }
    
    class func getAppLanguageWithCapital() -> String{
        if UserDefaults.standard.value(forKey: "App_Language") == nil {
            return "0"
        } else if UserDefaults.standard.value(forKey: "App_Language") as! String == "English"{
            return "En"
        } else {
            return "Ar"
        }
    }
    
}

extension UIColor {
    public convenience init(hex:String) {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        var r: CGFloat = 0.0
        var g: CGFloat = 0.0
        var b: CGFloat = 0.0
        var a: CGFloat = 1.0
        
        var rgbValue:UInt64 = 0
        Scanner(string: cString).scanHexInt64(&rgbValue)
        
        if ((cString.count) == 8) {
            r = CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0
            g =  CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0
            b = CGFloat((rgbValue & 0x0000FF)) / 255.0
            a = CGFloat((rgbValue & 0xFF000000)  >> 24) / 255.0
            
        } else if ((cString.count) == 6){
            r = CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0
            g =  CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0
            b = CGFloat((rgbValue & 0x0000FF)) / 255.0
            a =  CGFloat(1.0)
        }
        
        self.init(  red: r,
                    green: g,
                    blue: b,
                    alpha: a
        )
    }
}

