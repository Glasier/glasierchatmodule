//
//  PhotoPicker.swift
//  OETforNurses
//
//  Created by iOS Developer on 20/02/23.
//  Copyright © 2023 MVD. All rights reserved.
//

import UIKit
import MobileCoreServices

public class PhotoPicker: NSObject {
    
    public enum PickerSourceType: Int {
        case Camera = 0,
             PhotoLibrary,
             CameraAndPhotoLibrary,
             Video,
             Audio
    }
    
    var successBlock:((_ originalPhoto:UIImage?, _ editedPhoto: UIImage?, _ strUrl: String?) -> ())!
    
    public func pick(allowsEditing:Bool = false,
                     pickerSourceType: PickerSourceType = .PhotoLibrary,
                     controller: UIViewController,
                     successBlock success: @escaping ((_ originalPhoto:UIImage?, _ editedPhoto: UIImage?, _ strUrl: String?) -> ())) {
        
        if pickerSourceType == .CameraAndPhotoLibrary {
            
            let alertController = UIAlertController(title: "Select", message: "Source Type", preferredStyle: .actionSheet)
            alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { action in
                print("User pressed Cancel")
            }))
            
            alertController.addAction(UIAlertAction(title: "Take photo", style: .default, handler: { action in
                self.pick(allowsEditing: allowsEditing, pickerSourceType: .Camera, controller: controller, successBlock: success)
            }))
            
            alertController.addAction(UIAlertAction(title: "Choose photo", style: .default, handler: { action in
                self.pick(allowsEditing: allowsEditing, pickerSourceType: .PhotoLibrary, controller: controller, successBlock: success)
            }))
            
            controller.present(alertController, animated: true, completion: nil)
            
            return
            
        }
        
        //Now show the Image Picker Controller
        
        var sourceType:UIImagePickerController.SourceType!
        let picker = UIImagePickerController()
        
        switch pickerSourceType {
        case .Camera:
            
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                sourceType = .camera
            } else {
                sourceType = .savedPhotosAlbum
            }
            
        case .PhotoLibrary:
            sourceType = .photoLibrary
        case .Video:
            sourceType = .photoLibrary
            picker.mediaTypes = [kUTTypeMovie as String]
        case .Audio:
            sourceType = .photoLibrary
            picker.mediaTypes = [kUTTypeAudio as String]
        default:
            sourceType = .savedPhotosAlbum
        }
        
        picker.sourceType = sourceType
        picker.allowsEditing = allowsEditing
        picker.delegate = self
        
        self.successBlock = success
        
        controller.present(picker, animated: true, completion: nil)
        
    }
    
}

extension PhotoPicker: UINavigationControllerDelegate {
    
}

extension PhotoPicker: UIImagePickerControllerDelegate {
    
    public func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        let originalPhoto = info[UIImagePickerController.InfoKey(rawValue: UIImagePickerController.InfoKey.originalImage.rawValue) ] as? UIImage
        let editedPhoto = info[UIImagePickerController.InfoKey(rawValue: UIImagePickerController.InfoKey.editedImage.rawValue) ] as? UIImage
        
        if let videoURL = info[UIImagePickerController.InfoKey.mediaURL] as? NSURL {
            successBlock(originalPhoto, editedPhoto, videoURL.absoluteString)
        } else {
            successBlock(originalPhoto, editedPhoto, "")
        }
        
        picker.dismiss(animated: true, completion: nil)
        
    }
    
}
