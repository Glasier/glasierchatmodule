//
//  Model_ChatUserList.swift
//  OETforNurses
//
//  Created by iOS Developer on 27/02/23.
//  Copyright © 2023 MVD. All rights reserved.
//

import UIKit

class Model_ChatUserList: NSObject {
    
    var struser_id = ""
    var strtype = ""
    var strstatus = ""
    var struser_name = ""
    
    override init() {
        super.init()
    }
    
    init(dictionary:[String:Any]) {
        
        if let type = dictionary["type"] {
            self.strtype = String(describing: type).removeNullFromString()
        }
        if let user_id = dictionary["user_id"] {
            self.struser_id = String(describing: user_id).removeNullFromString()
        }
        if let status = dictionary["status"] {
            self.strstatus = String(describing: status).removeNullFromString()
        }
        if let user_name = dictionary["user_name"] {
            self.struser_name = String(describing: user_name).removeNullFromString()
        }
    
    }
    
    func getDictionary() -> [String: Any] {
        
        var dictionary = [String: Any]()
        
        dictionary["type"] = self.strtype
        dictionary["user_id"] = self.struser_id
        
        dictionary["status"] = self.strstatus
        dictionary["user_name"] = self.struser_name
        
        return dictionary
    }

}
