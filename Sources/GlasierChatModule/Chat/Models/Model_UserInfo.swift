//
//  Model_UserInfo.swift
//  MyFramework
//
//  Created by iOS Developer on 07/03/23.
//  Copyright © 2023 GirAppe Studio. All rights reserved.
//

import UIKit

class Model_UserInfo: NSObject {

    var straccount_name = ""
    var strcountry_code = ""
    var strcountry_name = ""
    var stremail = ""
    var strid = ""
    var stris_fast_track = ""
    var strlang_id = ""
    var strphone = ""
    var strprofile = ""
    var strqualification = ""
    var strregistered = ""
    var struser_status = ""
    
    override init() {
        super.init()
    }
    
    init(dictionary:[String:Any]) {
        
        if let account_name = dictionary["account_name"] {
            self.straccount_name = String(describing: account_name).removeNullFromString()
        }
        if let country_code = dictionary["country_code"] {
            self.strcountry_code = String(describing: country_code).removeNullFromString()
        }
        if let country_name = dictionary["country_name"] {
            self.strcountry_name = String(describing: country_name).removeNullFromString()
        }
        if let email = dictionary["email"] {
            self.stremail = String(describing: email).removeNullFromString()
        }
        if let id = dictionary["id"] {
            self.strid = String(describing: id).removeNullFromString()
        }
        if let is_fast_track = dictionary["is_fast_track"] {
            self.stris_fast_track = String(describing: is_fast_track).removeNullFromString()
        }
        if let lang_id = dictionary["lang_id"] {
            self.strlang_id = String(describing: lang_id).removeNullFromString()
        }
        if let phone = dictionary["phone"] {
            self.strphone = String(describing: phone).removeNullFromString()
        }
        if let profile = dictionary["profile"] {
            self.strprofile = String(describing: profile).removeNullFromString()
        }
        if let qualification = dictionary["qualification"] {
            self.strqualification = String(describing: qualification).removeNullFromString()
        }
        if let registered = dictionary["registered"] {
            self.strregistered = String(describing: registered).removeNullFromString()
        }
        if let user_status = dictionary["user_status"] {
            self.struser_status = String(describing: user_status).removeNullFromString()
        }
         
    }
    
    func getDictionary() -> [String: Any] {
        
        var dictionary = [String: Any]()
        
        dictionary["account_name"] = self.straccount_name
        dictionary["country_code"] = self.strcountry_code
        dictionary["country_name"] = self.strcountry_name
        dictionary["email"] = self.stremail
        dictionary["id"] = self.strid
        dictionary["is_fast_track"] = self.stris_fast_track
        dictionary["lang_id"] = self.strlang_id
        dictionary["phone"] = self.strphone
        dictionary["profile"] = self.strprofile
        dictionary["qualification"] = self.strqualification
        dictionary["registered"] = self.strregistered
        dictionary["user_status"] = self.struser_status
        
        return dictionary
    }
    
    
}
