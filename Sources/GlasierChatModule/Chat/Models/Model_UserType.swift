//
//  Model_UserType.swift
//  OETforNurses
//
//  Created by iOS Developer on 17/02/23.
//  Copyright © 2023 MVD. All rights reserved.
//

import UIKit

class Model_UserType: NSObject {
    
    var strtype = ""
    var struser_id = ""
    
    override init() {
        super.init()
    }
    
    init(dictionary:[String:Any]) {
        
        if let type = dictionary["type"] {
            self.strtype = String(describing: type).removeNullFromString()
        }
        if let user_id = dictionary["user_id"] {
            self.struser_id = String(describing: user_id).removeNullFromString()
        }
        
    }
    
    func getDictionary() -> [String: Any] {
        
        var dictionary = [String: Any]()
        
        dictionary["type"] = self.strtype
        dictionary["user_id"] = self.struser_id
        
        return dictionary
    }
    
}
