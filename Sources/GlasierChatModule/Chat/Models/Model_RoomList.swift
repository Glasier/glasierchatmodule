//
//  Model_RoomList.swift
//  OETforNurses
//
//  Created by iOS Developer on 17/02/23.
//  Copyright © 2023 MVD. All rights reserved.
//

import UIKit

class Model_RoomList: NSObject {

    var strroom_id = ""
    var strroom_name = ""

    override init() {
        super.init()
    }

    init(dictionary:[String:Any]) {
        
        if let room_id = dictionary["room_id"] {
            self.strroom_id = String(describing: room_id).removeNullFromString()
        }
        if let room_name = dictionary["room_name"] {
            self.strroom_name = String(describing: room_name).removeNullFromString()
        }

    }

    func getDictionary() -> [String: Any] {
        
        var dictionary = [String: Any]()
        
        dictionary["room_id"] = self.strroom_id
        dictionary["room_name"] = self.strroom_name
        
        return dictionary
    }
}

extension String {
    func removeNullFromString() -> String {
        
        if self != "<null>" {
            return self
        }
        return ""
    }
}
