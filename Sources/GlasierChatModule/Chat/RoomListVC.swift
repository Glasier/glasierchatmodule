//
//  RoomListVC.swift
//  OETforNurses
//
//  Created by iOS Developer on 17/02/23.
//  Copyright © 2023 MVD. All rights reserved.
//

import UIKit

class RoomListVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    //MARK: - Outside variables
    
    //MARK: - Insdie variables
    var arrRoomList = [Model_RoomList]()
    var objUserType = Model_UserType()
    
    //MARK: - IBOutlets
    @IBOutlet weak var tblRoomList: UITableView!
    @IBOutlet weak var imgBack: UIImageView!
    @IBOutlet weak var imgBG: UIImageView!
    
    //MARK: - UIView related methods
    override func viewDidLoad() {
        super.viewDidLoad()
        print("Call RoomListVC viewDidLoad function")
        self.setUpView()
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: Bundle.module)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - Setup view
    func setUpView() {
        
        self.tblRoomList.delegate = self
        self.tblRoomList.dataSource = self
        
//        self.tblRoomList.register(UINib(nibName: "CellT_RoomList", bundle: Bundle(for:CellT_RoomList.self)), forCellReuseIdentifier: "CellT_RoomList")
        self.tblRoomList.register(UINib(nibName: "CellT_RoomList", bundle: Bundle.module), forCellReuseIdentifier: "CellT_RoomList")
        DispatchQueue.main.async {
            self.serviceCall_GetLoginType()
            //            self.serviceCall_GetRoomList()
        }
        
        self.imgBack.image = SCImage(named: "ic_Back")
        self.imgBG.image = SCImage(named: "ic_AppBG")
        
    }
    
    //MARK: - IBActions
    
    @IBAction func btn_Back(sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func btn_Next(sender: UIButton) {
        
        let objCell = self.arrRoomList[sender.tag]
        
        let objChatVC = ChatVC(nibName: "ChatVC", bundle:
                                Bundle.module)
        objChatVC.strTitle = objCell.strroom_name
        objChatVC.objUserType = self.objUserType
        objChatVC.arrRoomList = objCell
        self.navigationController?.pushViewController(objChatVC, animated: true)
        
    }
    
    //MARK: - UItableview datasourse and delegate methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrRoomList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = self.tblRoomList.dequeueReusableCell(withIdentifier: "CellT_RoomList", for: indexPath) as? CellT_RoomList {
            
            let objCell = self.arrRoomList[indexPath.row]
            
            cell.lblRoomName.text = objCell.strroom_name
            
//            cell.bgView.topColor = UIColor.brown
//            cell.bgView.bottomColor = MyPublicClass().topColor
            
            cell.btnTab.tag = indexPath.row
            cell.btnTab.addTarget(self, action: #selector(self.btn_Next(sender:)), for: .touchUpInside)
            
            return cell
        }
        
        return UITableViewCell()
    }
    
    //MARK: ServiceCall
    func serviceCall_GetLoginType() {
        
        let param = ["email": objUserInfo.stremail, "fcm_token":strfcm_token, "app_name":strapp_name, "device_type":"ios"]
        
        NetworkManager.shared.postRequest(urlString: API.Get_Login_Type, params: param, view: self.view) { json in
            print(json)
            
            if (json as AnyObject).value(forKey: "status") as? Bool ?? false  {

                if let userArr = (json as AnyObject).value(forKey: GlobsVar.kkeyUser) as? [NSDictionary], userArr.count > 0 {
                    if let  objUser = userArr[0] as? [String : Any] {
                        self.objUserType = Model_UserType(dictionary: objUser)

                        UserDefaults.standard.set(objUser, forKey: GlobsVar.userDefaultKeyForGetUser)
                        UserDefaults.standard.synchronize()
                        
                        self.serviceCall_GetRoomList()
                    }
                }

            } else {
                //                    self.showAlert(title: GlobsVar.AppName, message: response.value(forKey: GlobsVar.kkeyMessage) as? String ?? "Some error!!!" , linkHandler: nil)
            }
            
        } failure: { error in
            print(error.localizedDescription)
        }
        
    }
    
    func serviceCall_GetRoomList() {
        
//        let obj = self.objUserType.getDictionary()
        
//        let parameters = [
//            [
//                "key": "user_id",
//                "value": self.objUserType.struser_id,
//                "type": "text"
//            ],
//            [
//                "key": "type",
//                "value": self.objUserType.strtype,
//                "type": "text"
//            ]] as [[String : Any]]
        
        let param = ["user_id": self.objUserType.struser_id, "type": self.objUserType.strtype]
        DispatchQueue.main.async {
            
            
            NetworkManager.shared.postRequest(urlString: API.Class_Room_List, params: param, view: self.view) { json in
                print(json)
                
                if (json as AnyObject).value(forKey: "status") as? Bool ?? false  {
                    
                    if let arrRoom = (json as AnyObject).value(forKey: GlobsVar.kkeyRoom_List) as? [NSDictionary], arrRoom.count > 0 {
                        
                        for room in arrRoom {
                            if let  newRoom = room as? [String : Any] {
                                self.arrRoomList.append(Model_RoomList(dictionary: newRoom))
                            }
                        }
                        DispatchQueue.main.async {
                            self.tblRoomList.reloadData()
                        }
                        
                    }
                    
                } else {
                    //                    self.showAlert(title: GlobsVar.AppName, message: response.value(forKey: GlobsVar.kkeyMessage) as? String ?? "Some error!!!" , linkHandler: nil)
                }
                
            } failure: { error in
                print(error.localizedDescription)
            }
            
            
        }
        
    }
    
}

struct GlobsVar {
    
    static var kkeyUser = "user"
    static var kkeyRoom_List = "room_list"
    
    static var userDefaultKeyForGetUser = "getUser"
}

