//
//  MyPublicClass.swift
//  MyFramework
//
//  Created by Andrzej Michnia on 14/02/2019.
//  Copyright © 2019 GirAppe Studio. All rights reserved.
//

import Foundation
import UIKit
//import JitsiMeetSDK

var objUserInfo = Model_UserInfo()
var strfcm_token = ""
var strapp_name = ""
var strJitsiURL = ""

public class MyPublicClass {
//    private let privateClass: MyPrivateClass

    var topColor = UIColor.init(hex: "006fab")
    var bottomColor = UIColor.init(hex: "004472" )
    private let biometricIDAuth = BiometricIDAuth()
    
    public init() {
//        privateClass = Factory().createMyPrivateClass()
    }

    public func showRoomListVC(controller: UIViewController, userInfo: NSDictionary, fcm_token: String, app_name: String) {

        strfcm_token = fcm_token
        strapp_name = app_name
        
        objUserInfo = Model_UserInfo(dictionary: userInfo as! [String : Any]);
        print("self.userInfo.getDictionary = ",objUserInfo.getDictionary())
        let oPController = RoomListVC(nibName: "RoomListVC", bundle:
            Bundle(for:RoomListVC.self))
        controller.navigationController?.pushViewController(oPController, animated: true)
    }
    
    public func showChatVC(controller: UIViewController, roomDetails: [String : Any], fcm_token: String, app_name: String) {

        print("Show Chat VC Function Call...")
        
        strfcm_token = fcm_token
        strapp_name = app_name
        
        if let userDetails = UserDefaults.standard.value(forKey:GlobsVar.userDefaultKeyForGetUser) as? [String: Any] {
            print("Framework UserDetails from userDefault = ",userDetails)
            var userType = Model_UserType()
            userType = Model_UserType(dictionary: userDetails)
            let objCell = Model_RoomList(dictionary: roomDetails)

            let objChatVC = ChatVC(nibName: "ChatVC", bundle:
                                    Bundle(for:ChatVC.self))
            objChatVC.strTitle = objCell.strroom_name
            objChatVC.objUserType = userType
            objChatVC.arrRoomList = objCell
            controller.navigationController?.pushViewController(objChatVC, animated: true)
        }
    }
    
    public func setSocketURL(strURL: String) {
        kHost = strURL
    }
    
    public func setJitsiVideoAndAudioCallURL(strURL: String) {
        strJitsiURL = strURL
    }
    
    public func setBiometricAuthentication(vc: UIViewController) {
        biometricIDAuth.canEvaluate { (canEvaluate, _, canEvaluateError) in
            guard canEvaluate else {
                alert(title: "Error",
                      message: canEvaluateError?.localizedDescription ?? "Face ID/Touch ID may not be configured",
                      okActionTitle: "Darn!", vc: vc)
                return
            }
            
            biometricIDAuth.evaluate { [weak self] (success, error) in
                guard success else {
                    self?.alert(title: "Error",
                                message: error?.localizedDescription ?? "Face ID/Touch ID may not be configured",
                                okActionTitle: "Darn!", vc: vc)
                    return
                }
                
                self?.alert(title: "Success",
                            message: "You have a free pass, now",
                            okActionTitle: "Yay!", vc: vc)
            }
        }
    }
    
    func serviceCall_GetLoginType(controller: UIViewController, userInfo: NSDictionary, fcm_token: String, app_name: String, completionHandler:  ((_ status:Bool,_ result:Any?) -> ())?) {
        
        let param = ["email": objUserInfo.stremail, "fcm_token":fcm_token, "app_name":app_name, "device_type":"ios"]
        
        NetworkManager.shared.postRequest(urlString: API.Get_Login_Type, params: param, view: controller.view) { json in
            print(json)
            
            if let status = (json as AnyObject).value(forKey: "status") as? Bool {

                DispatchQueue.main.async(execute: {completionHandler?(status,json)})

            } else {
                DispatchQueue.main.async(execute: {completionHandler?(false,json)})
            }
            
        } failure: { error in
            print(error.localizedDescription)
            DispatchQueue.main.async(execute: {completionHandler?(false,error.localizedDescription)})
        }
        
    }
    
    func alert(title: String, message: String, okActionTitle: String, vc: UIViewController) {
        let alertView = UIAlertController(title: title,
                                          message: message,
                                          preferredStyle: .alert)
        let okAction = UIAlertAction(title: okActionTitle, style: .default)
        alertView.addAction(okAction)
        vc.present(alertView, animated: true)
    }
}

public func SCImage(named name: String) -> UIImage? {
  UIImage(named: name, in: Bundle.module, compatibleWith: nil)
}
