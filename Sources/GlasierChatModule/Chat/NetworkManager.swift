//
//  NetworkManager.swift
//  MyFramework
//
//  Created by iOS Developer on 03/03/23.
//  Copyright © 2023 GirAppe Studio. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation

extension Data {
    
    mutating func appendString(str:String) {
        if let data = str.data(using: .utf8, allowLossyConversion: true) {
            append(data)
        }
    }
}

struct API {
    
    static let baseURL = "https://www.medicsportal.org/class_room_webservice/"
    
    static var Get_Login_Type = "get_login_type"
    static var Class_Room_List = "class_room_list"
    static var Get_Classroom_Chat_Message_List = "get_classroom_chat_message_list"
    static var Upload_attach_file = "upload_attach_file"
}

class NetworkManager {
    
    static let shared = NetworkManager(baseURL: URL(string:API.baseURL)!)
    
    let baseURL : URL
    
    var activityIndicator : UIActivityIndicatorView?
    
    typealias JSONDictionary = [String : Any]
    typealias JSONArray = [Any]
    
    typealias SuccessHandler = (_ json : Any) -> ()
    typealias ImageSuccessHandler = (_ image : UIImage) -> ()
    typealias ErrorHandler = (_ error : Error) -> ()
    
    lazy var defaultSession:URLSession = {
        
        let config = URLSessionConfiguration.default
        config.httpAdditionalHeaders = ["Content-Type":"application/json; charset=UTF-8"]
        return URLSession(configuration: config, delegate: nil, delegateQueue: nil)
        
    }()
    
    lazy var ephemeralSession:URLSession = {
        let config = URLSessionConfiguration.ephemeral
        return URLSession(configuration: config, delegate: nil, delegateQueue: nil)
    }()
    
    lazy var backgroundSession:URLSession = {
        let config = URLSessionConfiguration.background(withIdentifier: "background")
        return URLSession(configuration: config, delegate: nil, delegateQueue: nil)
    }()
    
    lazy var customSession:URLSession = {
        let config = URLSessionConfiguration.default
        config.urlCache = URLCache(memoryCapacity: 1000, diskCapacity: 10000, diskPath: "/")
        config.allowsCellularAccess = false
        config.httpAdditionalHeaders = ["Content-Type":"application/json; charset=UTF-8"]
        return URLSession(configuration: config, delegate: nil, delegateQueue: nil)
    }()
    
    private init(baseURL:URL){
        self.baseURL = baseURL
    }
    
    func getRequest(urlString:String,
                    view:UIView,
                    success: @escaping (SuccessHandler),
                    failure: @escaping (ErrorHandler)) {
        
        showProgressView(in: view)
        
        let url = self.baseURL.appendingPathComponent(urlString)
        
        let urlRequest = URLRequest(url: url)
        
        let task = defaultSession.dataTask(with: urlRequest, completionHandler: { (data,response,error) -> () in
            
            self.hideProgressView()
            
            guard error == nil else {
                failure(error!)
                return
            }
            
            if let aData = data,
                let urlResponse = response as? HTTPURLResponse,
                (200..<300).contains(urlResponse.statusCode) {
                
                do {
                    let responseJSON = try JSONSerialization.jsonObject(with: aData, options: [])
                    success(responseJSON)
                }
                catch let error as NSError {
                    failure(error)
                }
            }
        })
        task.resume()
        
    }
    
    func postRequest(urlString: String,
                     params:[String : Any],
                     view:UIView,
                     success:@escaping (SuccessHandler),
                     failure:@escaping (ErrorHandler)) {
        
        showProgressView(in: view)
        
        let url = self.baseURL.appendingPathComponent(urlString)
        print("API URL: \(url)")
        print("Params: \(params)")
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = "POST"
        let boundary = self.generateBoundaryString()
        urlRequest.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        urlRequest.setValue("application/json", forHTTPHeaderField: "Accept")
        
        urlRequest.networkServiceType = .default
        urlRequest.cachePolicy = .reloadRevalidatingCacheData
        urlRequest.timeoutInterval = 100
        urlRequest.httpShouldHandleCookies = true
        urlRequest.httpShouldUsePipelining = false
        urlRequest.allowsCellularAccess = true
        
        let body = self.createUploadDataForAPI(boundary: boundary, params: params)
        urlRequest.httpBody = body
        
        let task = defaultSession.dataTask(with: urlRequest, completionHandler: {(data, response, error) -> () in
            
            self.hideProgressView()
            
            guard error == nil else {
                failure(error!)
                return
            }
            
            if let aData = data,
            let urlResponse = response as? HTTPURLResponse,
                (200..<300).contains(urlResponse.statusCode) {
                
                do {
                    let responseJSON = try JSONSerialization.jsonObject(with: aData, options: [])
                    success(responseJSON)
                }
                catch let error as NSError {
                    failure(error)
                }
            }
            
        })
        
        task.resume()
    }
    
    
    func downloadImageFile(urlString:String,
                           view:UIView,
                           success:@escaping (ImageSuccessHandler),
                           failure:@escaping (ErrorHandler) ) {
    
        let url = URL(string: urlString)
        
        guard let unwrappedURL = url else {
            return
        }
        
        showProgressView(in: view)
        
        let task = defaultSession.downloadTask(with: unwrappedURL, completionHandler: {(localURL, response, error) in
            
            self.hideProgressView()
            
            guard error == nil else {
                failure(error!)
                return
            }
            
            if let fileURL = localURL {
                
                do {
                    let imageData = try Data(contentsOf: fileURL)
                    if let image = UIImage(data: imageData) {
                        success(image)
                    }
                    
                }
                catch let error as NSError {
                    failure(error)
                }
            }
            
        })
        
        task.resume()
    }
    
    func downloadImageInMemory(urlString:String,
                               view:UIView,
                               success: @escaping (ImageSuccessHandler),
                               failure: @escaping (ErrorHandler)) {
        
        let url = URL(string: urlString)
        
        guard let unwrapped = url else {return}
        
        showProgressView(in: view)
        
        let task = defaultSession.dataTask(with: unwrapped, completionHandler: {(data,response,error) in
            
            self.hideProgressView()
            
            guard error == nil else {
                failure(error!)
                return
            }
            
            if let aData = data,
            let urlResponse = response as? HTTPURLResponse,
                (200..<300).contains(urlResponse.statusCode) {
                
                if let image = UIImage(data: aData){
                    success(image)
                }
            }
        })
        
        task.resume()
    }
    
    func showProgressView(in view:UIView) {
        
        DispatchQueue.main.async {
            self.activityIndicator = UIActivityIndicatorView(style: .gray)
            self.activityIndicator?.frame = view.bounds
            if let progressBar = self.activityIndicator{
                view.addSubview(progressBar)
            }
            self.activityIndicator?.startAnimating()
        }
    }
    
    func hideProgressView() {
        DispatchQueue.main.async {
            self.activityIndicator?.stopAnimating()
            self.activityIndicator?.removeFromSuperview()
        }
    }
    
    func generateBoundaryString() -> String {
        return "Boundary-\(UUID().uuidString)"
    }
    
    func createUploadData(boundary:String,
                          params:[String:Any]?,
                          fileName:String,
                          keyName:String,
                          mimeType:String,
                          imageData:Data) -> Data {
        
        var body = Data()
        
        if let parameters = params {
            for (key,value) in parameters {
                body.appendString(str: "--\(boundary)\r\n")
                body.appendString(str: "Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
                body.appendString(str: "\(value)\r\n")
            }
            
            body.appendString(str: "--\(boundary)\r\n")
            body.appendString(str: "Content-Disposition: form-data; name=\"\(keyName)\"; filename=\"\(fileName)\"\r\n")
            body.appendString(str: "Content-Type: \(mimeType)\r\n\r\n")
            body.append(imageData)
            body.appendString(str: "\r\n")
            body.appendString(str: "--\(boundary)\r\n")
            
        }
        return body
    }
    
    func createUploadDataForAPI(boundary:String,
                          params:[String:Any]?) -> Data {
        
        var body = Data()
        
        if let parameters = params {
            for (key,value) in parameters {
                body.appendString(str: "--\(boundary)\r\n")
                body.appendString(str: "Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
                body.appendString(str: "\(value)\r\n")
            }
            
        }
        return body
    }
    
    
    func uploadData(urlString:String,
                    parameters:[String:Any]?,
                    imageFilename:String,
                    imageKeyName:String,
                    imageMimeType:String,
                    imageData:Data,
                    completionHandler:  ((_ status:Bool,_ result:Any?) -> ())?) {
        
//        if let url = URL(string: urlString) {
        let url = self.baseURL.appendingPathComponent(urlString)
            let boundary = self.generateBoundaryString()
            var urlRequest = URLRequest(url: url)
            
            print("API URL: \(url)")
            print("Params: \(parameters)")
            
            urlRequest.httpMethod = "POST"
            urlRequest.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
            urlRequest.setValue("application/json", forHTTPHeaderField: "Accept")
//            urlRequest.setValue("Client-ID \(ImageAPI.clientID)", forHTTPHeaderField: "Authorization")
            
            let body = self.createUploadData(boundary: boundary, params: parameters, fileName: imageFilename, keyName: imageKeyName, mimeType: imageMimeType, imageData: imageData)
            urlRequest.httpBody = body
            
            URLSession.shared.dataTask(with: urlRequest, completionHandler: {(data,response,error) in
                
                guard error == nil else {
                    DispatchQueue.main.async(execute: { completionHandler?(false,error)} )
                    return
                }
                
                if let aData = data {
                    do {
                        let responseJSON = try JSONSerialization.jsonObject(with: aData, options: [])
                        DispatchQueue.main.async(execute: {completionHandler?(true,responseJSON)})
                    }
                    catch let error as NSError {
                        DispatchQueue.main.async(execute: {completionHandler?(false,error)} )
                    }
                }
                
            }).resume()
//        }
//        else {
//            DispatchQueue.main.async(execute: {completionHandler?(false, nil)})
//        }
        
    }
    
    func getVideoThumbnail(url: URL) -> UIImage? {
        //let url = url as URL
        let request = URLRequest(url: url)
        let cache = URLCache.shared
        
        if let cachedResponse = cache.cachedResponse(for: request), let image = UIImage(data: cachedResponse.data) {
            return image
        }
        var image: UIImage?
        DispatchQueue.global().async {
            let asset = AVAsset(url: url)
            let imageGenerator = AVAssetImageGenerator(asset: asset)
            imageGenerator.appliesPreferredTrackTransform = true
            imageGenerator.maximumSize = CGSize(width: 250, height: 120)
            
            var time = asset.duration
            time.value = min(time.value, 2)
            
            do {
                let cgImage = try imageGenerator.copyCGImage(at: time, actualTime: nil)
                image = UIImage(cgImage: cgImage)
            } catch { }
            
            if let image = image, let data = image.pngData(), let response = HTTPURLResponse(url: url, statusCode: 200, httpVersion: nil, headerFields: nil) {
                let cachedResponse = CachedURLResponse(response: response, data: data)
                cache.storeCachedResponse(cachedResponse, for: request)
            }
        }
        return image
    }

    func UserImageForAnnotation() -> UIImage {

            let userPinImg : UIImage = UIImage(named: "audio_file")!
            UIGraphicsBeginImageContextWithOptions(userPinImg.size, false, 0.0);

        userPinImg.draw(in: CGRect(origin: CGPointZero, size: userPinImg.size))

            let roundRect : CGRect = CGRectMake(2, 2, userPinImg.size.width-4, userPinImg.size.width-4)

            let myUserImgView = UIImageView(frame: roundRect)
            myUserImgView.image = UIImage(named: "audio_file")
            //        myUserImgView.backgroundColor = UIColor.blackColor()
            //        myUserImgView.layer.borderColor = UIColor.whiteColor().CGColor
            //        myUserImgView.layer.borderWidth = 0.5

            let layer: CALayer = myUserImgView.layer

            layer.masksToBounds = true
            layer.cornerRadius = myUserImgView.frame.size.width/2

        UIGraphicsBeginImageContextWithOptions(myUserImgView.bounds.size, myUserImgView.isOpaque, 0.0)
        layer.render(in: UIGraphicsGetCurrentContext()!)
            let roundedImage = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()

        roundedImage!.draw(in: roundRect)


        let resultImg : UIImage = UIGraphicsGetImageFromCurrentImageContext()!
            UIGraphicsEndImageContext()

            return resultImg

        }

//    func loadImageWithUrl(_ url: URL, completionBlock: @escaping (_ image: UIImage?)-> Void) {
    func loadImageWithUrl(_ url: URL, imageView: UIImageView, isFromSender: Bool) {
        let activityIndicator = UIActivityIndicatorView()
        
        var imageURL: URL?
        // setup activityIndicator...
        if #available(iOS 13.0, *) {
            activityIndicator.color = isFromSender ? .darkGray : .white
        } else {
            activityIndicator.color = isFromSender ? .darkGray : .white
        }

        imageView.addSubview(activityIndicator)
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        activityIndicator.center = imageView.center
        activityIndicator.centerXAnchor.constraint(equalTo: imageView.centerXAnchor).isActive = true
        activityIndicator.centerYAnchor.constraint(equalTo: imageView.centerYAnchor).isActive = true

        imageURL = url
        imageView.image = nil
        activityIndicator.isHidden = false
        activityIndicator.startAnimating()

        // retrieves image if already available in cache
        if let imageFromCache = imageCache.object(forKey: url as AnyObject) as? UIImage {
            
//            completionBlock(imageFromCache)
            DispatchQueue.main.async(execute: {
                activityIndicator.isHidden = true
                activityIndicator.stopAnimating()
            })
            imageView.image = imageFromCache
            return
        }

        // image does not available in cache.. so retrieving it from url...
        URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in
            if error != nil {
                print(error as Any)
                DispatchQueue.main.async(execute: {
                    activityIndicator.isHidden = true
                    self.activityIndicator?.stopAnimating()
                })
                return
            }
            DispatchQueue.main.async(execute: {
                if let unwrappedData = data, let imageToCache = UIImage(data: unwrappedData) {

                    if imageURL == url {
//                        completionBlock(imageToCache)
                        imageView.image = imageToCache
                    }

                    imageCache.setObject(imageToCache, forKey: url as AnyObject)
                }
                DispatchQueue.main.async(execute: {
                    activityIndicator.isHidden = true
                    self.activityIndicator?.stopAnimating()
                })
            })
        }).resume()
    }
    
}


//extension NetworkManager {
//  fileprivate func runOnMainThread(block:@escaping ()->Void) {
//    if Thread.isMainThread {
//      block()
//    } else {
//      let mainQueue = OperationQueue.main
//      mainQueue.addOperation({
//        block()
//      })
//    }
//  }
//}


let imageCache = NSCache<AnyObject, AnyObject>()

class ImageLoader: UIImageView {

    var imageURL: URL?

//    let activityIndicator = UIActivityIndicatorView()

    func loadImageWithUrl(_ url: URL) {

        print("Call loadImageWithUrl function...")
        
        // setup activityIndicator...
//        if #available(iOS 13.0, *) {
//            activityIndicator.color = .systemGray6
//        } else {
//            activityIndicator.color = .darkGray
//        }

//        addSubview(activityIndicator)
//        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
//        activityIndicator.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
//        activityIndicator.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true

        imageURL = url

        image = nil
//        activityIndicator.startAnimating()

        // retrieves image if already available in cache
        if let imageFromCache = imageCache.object(forKey: url as AnyObject) as? UIImage {
            print("if let imageFromCache = imageCache.object(forKey: url as AnyObject) as? UIImag")
            self.image = imageFromCache
//            activityIndicator.stopAnimating()
            return
        }

        // image does not available in cache.. so retrieving it from url...
        URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in
            print("URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in")
            if error != nil {
                print(error as Any)
                DispatchQueue.main.async(execute: {
//                    self.activityIndicator.stopAnimating()
                })
                return
            }
            print("URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in asdasdasdsad")
            DispatchQueue.main.async(execute: {
                print("DispatchQueue.main.async(execute: {")
                if let unwrappedData = data, let imageToCache = UIImage(data: unwrappedData) {

                    if self.imageURL == url {
                        self.image = imageToCache
                    }

                    imageCache.setObject(imageToCache, forKey: url as AnyObject)
                }
//                self.activityIndicator.stopAnimating()
            })
        }).resume()
    }
}

extension Date {

    func formatRelativeString() -> String {
        let dateFormatter = DateFormatter()
        let calendar = Calendar(identifier: .gregorian)
        dateFormatter.doesRelativeDateFormatting = true

        if calendar.isDateInToday(self) {
            dateFormatter.timeStyle = .short
            dateFormatter.dateStyle = .none
        } else if calendar.isDateInYesterday(self){
            dateFormatter.timeStyle = .none
            dateFormatter.dateStyle = .medium
        } else if calendar.compare(Date(), to: self, toGranularity: .weekOfYear) == .orderedSame {
            let weekday = calendar.dateComponents([.weekday], from: self).weekday ?? 0
            return dateFormatter.weekdaySymbols[weekday-1]
        } else {
            dateFormatter.timeStyle = .none
            dateFormatter.dateStyle = .short
        }

        return dateFormatter.string(from: self)
    }
}

extension String {

    func toDate(withFormat format: String = "yyyy-MM-dd HH:mm:ss")-> Date?{

        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.locale = Locale.current
//        dateFormatter.calendar = Calendar(identifier: .gregorian)
        dateFormatter.dateFormat = format
        let date = dateFormatter.date(from: self)

        return date

    }
}
extension Array {
  func sliced(by dateComponents: Set<Calendar.Component>, for key: KeyPath<Element, Date>) -> [Date: [Element]] {
    let initial: [Date: [Element]] = [:]
    let groupedByDateComponents = reduce(into: initial) { acc, cur in
      let components = Calendar.current.dateComponents(dateComponents, from: cur[keyPath: key])
      let date = Calendar.current.date(from: components)!
      let existing = acc[date] ?? []
      acc[date] = existing + [cur]
    }

    return groupedByDateComponents
  }
}
