// swift-tools-version: 5.7
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "GlasierChatModule",
    products: [
        // Products define the executables and libraries a package produces, and make them visible to other packages.
        .library(
            name: "GlasierChatModule",
            targets: ["GlasierChatModule"]),
    ],
    dependencies: [
        // Dependencies declare other packages that this package depends on.
        // .package(url: /* package url */, from: "1.0.0"),
        .package(url: "https://github.com/socketio/socket.io-client-swift", .upToNextMinor(from: "15.0.0"))
    ],
    targets: [
        // Targets are the basic building blocks of a package. A target can define a module or a test suite.
        // Targets can depend on other targets in this package, and on products in packages this package depends on.
        //Added local framework
        .binaryTarget(
                    name: "JitsiMeetSDK",
                    url: "https://gitlab.com/Glasier/glasierchatmodule/-/raw/main/JitsiMeetSDK.xcframework.zip",
                    checksum: "7fd5598477427cc2931189e547e96e55bbee0f84ae47514ad8c9db9012fa8494"
                ),
        .binaryTarget(
                    name: "GiphyUISDK",
                    url: "https://gitlab.com/Glasier/glasierchatmodule/-/raw/main/GiphyUISDK.xcframework.zip",
                    checksum: "44284d48e182a800c304d72025fa5f0ab38a1f1910220c9986646d5b2140c143"
                ),
        .binaryTarget(
                    name: "WebRTC",
                    url: "https://gitlab.com/Glasier/glasierchatmodule/-/raw/main/WebRTC.xcframework.zip",
                    checksum: "2da09240f6abefba4065b99ff2c8dc9be97f7573addb402106a12aee7f81e071"
                ),
        .target(
            name: "GlasierChatModule",
            dependencies: ["JitsiMeetSDK","GiphyUISDK","WebRTC"]),
        .testTarget(
            name: "GlasierChatModuleTests",
            dependencies: ["GlasierChatModule"]),
    ]
)

